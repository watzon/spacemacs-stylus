;;; packages.el --- stylus Layer packages File for Spacemacs
;;
;; Copyright (c) 2012-2014 Sylvain Benner
;; Copyright (c) 2014-2015 Sylvain Benner & Contributors
;;
;; Author: Sylvain Benner <sylvain.benner@gmail.com>
;; URL: https://github.com/syl20bnr/spacemacs
;;
;; This file is not part of GNU Emacs.
;;
;;; License: GPLv3

;; List of all packages to install and/or initialize. Built-in packages
;; which require an initialization must be listed explicitly in the list.
(setq stylus-packages
    '(
      stylus-mode
      ))

;; List of packages to exclude.
(setq stylus-excluded-packages '())

;; For each package, define a function stylus/init-<package-name>
;;
(defun stylus/init-stylus-mode ()
  (use-package stylus-mode
    :ensure t
    :commands stylus-mode
    :init (progn
            (add-to-list 'auto-mode-alist '("\\.styl\\'" . stylus-mode))))
  )
;;
;; Often the body of an initialize function uses `use-package'
;; For more info on `use-package', see readme:
;; https://github.com/jwiegley/use-package
